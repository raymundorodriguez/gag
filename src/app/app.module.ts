import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

// Components
import { LandingMainComponent } from './components/landing-main/landing-main.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ReviewComponent } from './components/review/review.component';
import { ContactComponent } from './components/contact/contact.component';


/// COMPONENTS
const COMPONETS = [
  LandingMainComponent,
  NavbarComponent,
  HomeComponent,
  AboutComponent,
  ProjectsComponent,
  ReviewComponent,
  ContactComponent,
];


@NgModule({
  declarations: [
    AppComponent,
    ...COMPONETS
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
